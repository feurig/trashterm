import wx
import trs80rom
# move these to trs80rom
TRS80HMARGIN=6
TRS80VMARGIN=6
TRS80COLS=64
TRS80ROWS=16
SCREENWIDTH=TRS80HMARGIN+(TRS80COLS*trs80rom.TRS80CHARW)+TRS80HMARGIN
SCREENHEIGHT=TRS80VMARGIN+(TRS80ROWS*trs80rom.TRS80CHARH)+TRS80VMARGIN
class Trs80Bits(wx.MemoryDC):
    def __init__(self, *args, **kwds):
        wx.MemoryDC.__init__(self, *args, **kwds)
        self.SelectObject(wx.Bitmap.FromBuffer(trs80rom.TRS80CHARW,
                                               trs80rom.TRS80CHARH*256,
                                               trs80rom.trs80charset)
                                               )

    def drawChar(self,target,ch,x,y):
        target.Blit(
           x, #Copy to this X coordinate
           y, #Copy to this Y coordinate
           trs80rom.TRS80CHARW, #Copy this width
           trs80rom.TRS80CHARH, #Copy this height
           self, #From where do we copy?
           0, #What's the X offset in the original DC?
           ord(ch) * trs80rom.TRS80CHARH #What's the Y offset in the original DC?
           )

class TrashTerm(wx.Window):
    def __init__(self, parent):
        wx.Window.__init__(self,parent)
        self.rows=TRS80ROWS
        self.cols=TRS80COLS
        self.screen = [[' ' for x in range(self.cols)] for y in range(self.rows)]
        self.background=wx.Brush(wx.Colour(trs80rom.TRS80BLACK))
        self.foreground=wx.Brush(wx.Colour(trs80rom.TRS80WHITE))
        self.charset=Trs80Bits()
        self.screenbuffer=wx.MemoryDC(wx.Bitmap(SCREENWIDTH,SCREENHEIGHT,depth=256))
        #adding strings is clunky. mebby think about this.
        x=20
        y=8
        for c in 'Hello World!':
            self.screen [y][x]=c
            x=x+1


        self.Bind(wx.EVT_PAINT,self.OnPaint)

    def OnPaint(self,evt):
        dc=wx.PaintDC(self)
        self.screenbuffer.SetBackground(self.background)
        self.screenbuffer.Clear() #fill dc with background Colour
        for y in range(self.rows):
            for x in range(self.cols):
                self.charset.drawChar(self.screenbuffer,
                                      self.screen[y][x],
                                      (x*trs80rom.TRS80CHARW)+TRS80HMARGIN,
                                      (y*trs80rom.TRS80CHARH)+TRS80VMARGIN
                                      )
        dc.StretchBlit(0, 0, SCREENWIDTH*2, SCREENHEIGHT*3, self.screenbuffer, 0, 0,
                       SCREENWIDTH,SCREENHEIGHT)
if __name__ == '__main__':
    dummy=wx.App()
    frame=wx.Frame(None,title="trashterm", size=(SCREENWIDTH*2,SCREENHEIGHT*3))
    term=TrashTerm(frame)
    frame.Show()
    dummy.MainLoop()
