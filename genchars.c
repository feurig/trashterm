
/********************************************************************genchars.c
*   Generate 125x12x6 array of pixels from trs-80 character rom definitions
*******************************************************************************
*
* Author: D Delmar Davis, Suspect Devices <don@suspectdevices.com>
*
* Liscense: CopyLeft 2020, D. Delmar Davis (Modified BSD as per README.md)
*
* Description:
* Creates an array of pixels to be used by a framebuffer based trs 80 lookalike.
* Uses the character definitions from the SDLTRS project and then fills in the
* trs80 graphics characters and uses the remaining space to create an inverted
* character set.
*
* Usage:
*  To create source file for chars run the following:
*    $ cc genchars.c trs_chars.c ;./a.out >trs80chars.c
*  To plot the chars out to an XPM2 file define XPM (! FOOBARRED !! See Below)
*    $ cc -DXPM genchars.c trs_chars.c ;./a.out >trs80chars.xpm
*  To plot the chars out to a python file define PYTHON
*    $ cc -DPYTHON genchars.c trs_chars.c ;./a.out >trs80rom.py
*  To plot the chars out using ascii define CHARPLOT
*    $ cc -DCHARPLOT genchars.c trs_chars.c ;./a.out |less
*
* Creates a file containing an array of characters (or rgb triplets for python)
* const char trs80chars[256][12][6] = {
*  {// ---- ---- ---- ---- ---- ---- 0
*     {0x00,0x00,0x00,0x00,0x00,0x00},
*     ...
*     {0x00,0x00,0x00,0x00,0x00,0x00},
*  },
* ...
* };
******************************************************************************/
#include <stdio.h>
#include "trs_iodefs.h"
#define CG 0
//#define CHARPLOT 1
#ifdef CHARPLOT
#define START_CHARSET /*start charset*/
#define START_CHAR printf("------ %d\n",ch);
#define START_ROW /*start row*/
#define PLOT_LSB(X) printf("%c",((X)&0x01)?'X':' ');
#define END_ROW printf("\n");
#define END_CHAR printf("\n");
#define END_CHARSET printf("------\n");
#elif defined(XPM)
//! FOOBARRED!! XPM2 can't be read by anyone. rewrite with XPM1 or XPM3. !#$@#*
#define START_CHARSET printf("! XPM2\n6 3072 2 1\na c #3b3b41\nb c #e6ffff\n");
#define START_CHAR /*start char*/
#define START_ROW /*start row*/
#define PLOT_LSB(X) printf("%c",((X)&0x01)?'a':'b');
#define END_ROW printf("\n");
#define END_CHAR /*end char*/
#define END_CHARSET /*end charset*/

// flesh this out manually and then backfill the class.
#elif defined(PYTHON)
#define FILE_HEADER "TRS80WHITE='#e6ffff'\nTRS80BLACK='#3b3b41'\nTRS80CHARW=6\nTRS80CHARH=12\n"
#define START_CHARSET printf(FILE_HEADER "trs80charset=bytearray([\n");
#define START_CHAR printf("  # -------------------------------------------------------------------------%d\n",ch);
#define START_ROW /*start row*/
#define PLOT_LSB(X) printf("%s",((X)&0x01)?"0xe6,0xff,0xff,":"0x3b,0x3b,0x41,");
//should get this from TRS80WHITE,TRS80BLACK--^  --^  --^     --^  --^  --^
#define END_ROW printf("\n");
#define END_CHAR /*end char*/
#define END_CHARSET printf("])\n");

#else

#define START_CHARSET  printf("const char trs80chars[256][12][6] = {\n");
#define START_CHAR printf(" {// ---- ---- ---- ---- ---- ----   %d\n",ch);
#define START_ROW  printf("   { ");
#define PLOT_LSB(X) printf("%s,",((X)&0x01)?"0xFF":"0x00");
#define END_ROW printf("},\n");
#define END_CHAR printf(" },\n");
#define END_CHARSET printf("};\n");
#endif
extern const char trs_char_data[][MAXCHARS][TRS_CHAR_HEIGHT];

int main(void) {

  int row, col, ch;
  char b,b1;

  START_CHARSET;
  // --- convert bit-encoded characters from SDLTRS code
  //   (which is copied from original trs-80 character roms)
  //
  for (ch=0;ch<128;ch++) {
    START_CHAR;
    for (row=0;row<12;row++) {
      START_ROW;
      b=trs_char_data[CG][ch][row];
      for (col=0;col<6;col++) {
        //printf("%c",(b&0x01)?'X':' ');
        PLOT_LSB(b);
        b>>=1;
      }
      END_ROW;
    }
    END_CHAR;

  }
  // ---------- Generate TRS-80 graphics characters ----------
  // Calculate bit patterns
  // 128 + 0 to 0x3f
  // 000111
  // 000111
  // 000111
  // 000111
  // 222444
  // 222444
  // 222444
  // 222444
  // 16x32x
  // 16x32x
  // 16x32x
  // 16x32x
  for (ch=128;ch<192;ch++) {
    START_CHAR;
    b1=ch-128;
    for (row=0;row<12;) {
      START_ROW;
      b=b1;
      for (col=0;col<6;) {
        PLOT_LSB(b);
        if (!(++col%3)) b>>=1;
      }
      if (!(++row%4)) b1>>=2;
      END_ROW;
    }
    END_CHAR;
  }

  //----------------- fill remaining charachters with inverse of the alpha
  for (ch=192;ch<256;ch++) {
    START_CHAR;
    for (row=0;row<12;row++) {
      START_ROW;
      b=~trs_char_data[CG][(ch-(192))+32][row];
      for (col=0;col<6;col++) {
        PLOT_LSB(b);
        b>>=1;
      }
      END_ROW;
    }
    END_CHAR;
  }
  END_CHARSET;

}
