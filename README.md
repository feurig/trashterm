# README If you Must...#

When I was 17 I was tasked with modeling atomic orbitals on a TRS80 Model 1. The graphics were these clunky characters made up of a 2x3 grid of 3x4 pixel blocks. Black and white was more like blue-green(e6ffff) and grey(3b3b41).

![TRS 80 Image from the internet](Tandy_TRS-80_Model_I.jpg)
### What is this repository for? ###

* I wanted my raspberry pi to look and feel like my first experiences with computers.
* Version 0.1 (moving to pygame)

### How do I build this? ###

* Generate characters based on the SDLTRS project

 
### Dependencies/Attributions ###
 * Fonts from [SDLTRS](https://github.com/RetroPie/sdltrs) project.
 * [PyGame 2.x](https://www.pygame.org/news)

### Liscence ###
CopyRight 2020 D. Delmar Davis, don@suspectdevices.com
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
### References ###

* [TRS-80 gharacter set](https://en.wikipedia.org/wiki/TRS-80_character_set)
* [Level III BASIC manual](https://ia800703.us.archive.org/21/items/Level_III_BASIC_1979_Microsoft/Level_III_BASIC_1979_Microsoft.pdf) 
* [Adafruit Pygame Framebuffer Tutorial](https://learn.adafruit.com/pi-video-output-using-pygame/pointing-pygame-to-the-framebuffer)